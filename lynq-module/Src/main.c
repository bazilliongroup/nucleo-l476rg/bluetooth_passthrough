/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <string.h>
#include <stdbool.h>
#include <stdio.h>
#include "TimerModule.h"

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
typedef enum commandCount
{
	COMMAND_ZERO,
	COMMAND_ONE,
	COMMAND_TWO,
	COMMAND_THREE,
	COMMAND_FOUR,
	COMMAND_FIVE,
	COMMAND_SIX,
	COMMAND_SEVEN,
	COMMAND_EIGHT,
	COMMAND_NINE,
	COMMAND_TEN,
	COMMAND_ELEVEN,
	COMMAND_TWELVE,
	COMMAND_THIRTEEN,
	COMMAND_FOURTEEN
}COMMAND_COUNT;

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define DEBUG
#define OFFSET 2
#define CRETURNS_NEWLINE 2

//Transmission protocol
#define START_OF_HEADING	0x01
#define START_OF_TEXT		0x02
#define END_OF_TEXT			0x03
#define END_OF_TRANSMISSION 0x04
#define NEW_LINE			0x0A
#define GET_ITEMS_CMD		0x11
#define SET_ITEMS_CMD		0x12
#define CARRIAGE_RETURN		0x0D
#define INVALID_CMD 		0xFF
#define TX_TERMINATOR		0xFF

//size of arrays
#define LYNQ_DEV_CONT_SIZE		 		01
#define LYNQ_HEADER_SIZE				01
#define LYNQ_START_TEXT_SIZE			01
#define LYNQ_END_TEXT_SIZE				01
#define LYNQ_ENDING_SIZE				01
#define LYNQ_CR_LF_SIZE					02
#define LYNQ_TERMINATOR_SIZE			01
#define LYNQ_ARG_SIZE					01
#define LYNQ_NAME_SIZE		20
#define LYNQ_DATA_SIZE		04
#define LYNQ_TOT_DATA_RX_SIZE	OFFSET + LYNQ_DEV_CONT_SIZE + LYNQ_HEADER_SIZE +\
								LYNQ_START_TEXT_SIZE + LYNQ_ARG_SIZE + LYNQ_NAME_SIZE + LYNQ_END_TEXT_SIZE + \
								LYNQ_START_TEXT_SIZE + LYNQ_ARG_SIZE + LYNQ_DATA_SIZE + LYNQ_END_TEXT_SIZE + \
								LYNQ_START_TEXT_SIZE + LYNQ_ARG_SIZE + LYNQ_DATA_SIZE + LYNQ_END_TEXT_SIZE + \
								LYNQ_START_TEXT_SIZE + LYNQ_ARG_SIZE + LYNQ_DATA_SIZE + LYNQ_END_TEXT_SIZE + \
								LYNQ_ENDING_SIZE

#define LYNQ_TOT_DATA_TX_SIZE	LYNQ_DEV_CONT_SIZE + LYNQ_HEADER_SIZE +\
								LYNQ_START_TEXT_SIZE + LYNQ_ARG_SIZE + LYNQ_NAME_SIZE + LYNQ_END_TEXT_SIZE + \
								LYNQ_START_TEXT_SIZE + LYNQ_ARG_SIZE + LYNQ_DATA_SIZE + LYNQ_END_TEXT_SIZE + \
								LYNQ_START_TEXT_SIZE + LYNQ_ARG_SIZE + LYNQ_DATA_SIZE + LYNQ_END_TEXT_SIZE + \
								LYNQ_START_TEXT_SIZE + LYNQ_ARG_SIZE + LYNQ_DATA_SIZE + LYNQ_END_TEXT_SIZE + \
								LYNQ_ENDING_SIZE + LYNQ_CR_LF_SIZE + LYNQ_TERMINATOR_SIZE

static uint8_t m_tmrId1 = UNSUCCESSFUL;
static uint8_t m_tmrId2 = UNSUCCESSFUL;
#define UART_RX_DATA_SIZE 1
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
TIM_HandleTypeDef htim17;

UART_HandleTypeDef huart1;
UART_HandleTypeDef huart3;
DMA_HandleTypeDef hdma_usart1_rx;
DMA_HandleTypeDef hdma_usart3_rx;

/* USER CODE BEGIN PV */
//****Remember "static" is used like "private" in C++*******//
static char m_rxData3[UART_RX_DATA_SIZE];	//to the PC
static char m_rxData1[UART_RX_DATA_SIZE];	//to the App

static UART_HandleTypeDef* m_huart3Ptr;

volatile static uint8_t m_lynqNameNumBytes;
volatile static uint8_t m_lynqFreqNumBytes;
volatile static uint8_t m_lynqLongNumBytes;
volatile static uint8_t m_lynqLatNumBytes;
volatile static uint8_t m_lynqName[LYNQ_NAME_SIZE];	//Lynq Name
volatile static uint8_t m_lynqFreq[LYNQ_DATA_SIZE];	//Lynq Frequency
volatile static uint8_t m_lynqLong[LYNQ_DATA_SIZE];	//Lynq longitude
volatile static uint8_t m_lynqLat[LYNQ_DATA_SIZE];	//Lynq latitude

volatile static int  m_numOfBytesForLynqId;
volatile static int  m_numOfBytesForFrequency;
volatile static int  m_numOfBytesForLongitude;
volatile static int  m_numOfBytesForLatitude;

volatile static uint8_t m_LynqCom;
volatile static char m_rxBuffer[LYNQ_TOT_DATA_RX_SIZE];
volatile static char m_txBuffer[LYNQ_TOT_DATA_TX_SIZE];

volatile static COMMAND_COUNT m_cmdCount;
volatile static int m_setLynqNameCounter;
volatile static int m_setByteCounter;
volatile static int m_rxBufferCounter;
volatile static int m_setDataLoc;

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_USART1_UART_Init(void);
static void MX_TIM17_Init(void);
static void MX_USART3_UART_Init(void);
/* USER CODE BEGIN PFP */

//****Remember "static" is used like "private" in C++*******//
static void m_Led1Function(uint8_t var);
static void m_ProcessCommand(uint8_t var);

static void m_CheckGetCmd();
static void m_CheckSetCmd();

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */
  

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_USART1_UART_Init();
  MX_TIM17_Init();
  MX_USART3_UART_Init();
  /* USER CODE BEGIN 2 */

  //Set DMA for UART receive
  HAL_UART_Receive_DMA(&huart3, (uint8_t *)&m_rxData3[0], UART_RX_DATA_SIZE);	//Receive from App
  HAL_UART_Receive_DMA(&huart1, (uint8_t *)&m_rxData1[0], UART_RX_DATA_SIZE);	//Receive from debug

  //Init variables
  m_huart3Ptr = &huart3;
  m_LynqCom = INVALID_CMD;
  m_cmdCount = COMMAND_ZERO;
  m_setLynqNameCounter = 0;
  m_setByteCounter = 0;
  m_rxBufferCounter = 0;
  m_numOfBytesForLynqId = 0;
  m_numOfBytesForFrequency = 0;
  m_numOfBytesForLongitude = 0;
  m_numOfBytesForLatitude = 0;
  m_setDataLoc = OFFSET;

  //Timer Init
  TmrInit(&htim17);
  HAL_TIM_Base_Start_IT(&htim17);

  m_tmrId1 = TmrCreate();
  TmrPopulate(m_tmrId1, 1000, m_Led1Function, 0, false);
  TmrStart(m_tmrId1);

  m_tmrId2  = TmrCreate();
  TmrPopulate(m_tmrId2, 1000, m_ProcessCommand, 0, false);
  TmrStart(m_tmrId2);

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */

  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 1;
  RCC_OscInitStruct.PLL.PLLN = 10;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV7;
  RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV2;
  RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_4) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART1|RCC_PERIPHCLK_USART3;
  PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK2;
  PeriphClkInit.Usart3ClockSelection = RCC_USART3CLKSOURCE_PCLK1;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure the main internal regulator output voltage 
  */
  if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief TIM17 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM17_Init(void)
{

  /* USER CODE BEGIN TIM17_Init 0 */

  /* USER CODE END TIM17_Init 0 */

  /* USER CODE BEGIN TIM17_Init 1 */

  /* USER CODE END TIM17_Init 1 */
  htim17.Instance = TIM17;
  htim17.Init.Prescaler = 40000;
  htim17.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim17.Init.Period = 1;
  htim17.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim17.Init.RepetitionCounter = 0;
  htim17.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim17) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM17_Init 2 */

  /* USER CODE END TIM17_Init 2 */

}

/**
  * @brief USART1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART1_UART_Init(void)
{

  /* USER CODE BEGIN USART1_Init 0 */

  /* USER CODE END USART1_Init 0 */

  /* USER CODE BEGIN USART1_Init 1 */

  /* USER CODE END USART1_Init 1 */
  huart1.Instance = USART1;
  huart1.Init.BaudRate = 9600;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  huart1.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart1.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART1_Init 2 */

  /* USER CODE END USART1_Init 2 */

}

/**
  * @brief USART3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART3_UART_Init(void)
{

  /* USER CODE BEGIN USART3_Init 0 */

  /* USER CODE END USART3_Init 0 */

  /* USER CODE BEGIN USART3_Init 1 */

  /* USER CODE END USART3_Init 1 */
  huart3.Instance = USART3;
  huart3.Init.BaudRate = 9600;
  huart3.Init.WordLength = UART_WORDLENGTH_8B;
  huart3.Init.StopBits = UART_STOPBITS_1;
  huart3.Init.Parity = UART_PARITY_NONE;
  huart3.Init.Mode = UART_MODE_TX_RX;
  huart3.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart3.Init.OverSampling = UART_OVERSAMPLING_16;
  huart3.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart3.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart3) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART3_Init 2 */

  /* USER CODE END USART3_Init 2 */

}

/** 
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void) 
{

  /* DMA controller clock enable */
  __HAL_RCC_DMA1_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA1_Channel3_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel3_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel3_IRQn);
  /* DMA1_Channel5_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel5_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel5_IRQn);

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(LED_GPIO_Port, LED_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : LED_Pin */
  GPIO_InitStruct.Pin = LED_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(LED_GPIO_Port, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  /* Prevent unused argument(s) compilation warning */
  UNUSED(htim);

  /* NOTE : This function Should not be modified, when the callback is needed,
            the __HAL_TIM_PeriodElapsedCallback could be implemented in the user file
   */

  TmrProcessTimerArray(htim);
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
  /* Prevent unused argument(s) compilation warning */
  UNUSED(huart);
  /* NOTE: This function Should not be modified, when the callback is needed,
           the HAL_UART_TxCpltCallback could be implemented in the user file
   */

  if (m_rxData1[0] == (char)GET_ITEMS_CMD || m_LynqCom == GET_ITEMS_CMD)
  {
	  m_CheckGetCmd();
  }
  else if (m_rxData1[0] == (char)SET_ITEMS_CMD || m_LynqCom == SET_ITEMS_CMD)
  {
	  m_CheckSetCmd();
  }

#ifdef DEBUG
	  //send to PC (for debugging purposes)
	  HAL_UART_Transmit(m_huart3Ptr, huart->pRxBuffPtr, huart->RxXferSize, 10);
#endif

}

void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart)
{
  /* Prevent unused argument(s) compilation warning */
  UNUSED(huart);

  /* NOTE: This function Should not be modified, when the callback is needed,
           the HAL_UART_TxCpltCallback could be implemented in the user file
   */

 	  HAL_UART_Transmit(huart, huart->pTxBuffPtr, huart->TxXferSize, 10);
}

static void m_Led1Function(uint8_t var)
{
    HAL_GPIO_TogglePin(LED_GPIO_Port, LED_Pin);
}

static void m_CheckGetCmd()
{
	if(m_rxData1[0] == (char)GET_ITEMS_CMD && m_cmdCount == COMMAND_ZERO)				//GET ITEMS COMMAND
	{
		m_LynqCom = GET_ITEMS_CMD;
		m_rxBuffer[m_setDataLoc] = m_rxData1[0];
		m_setDataLoc++;
		m_cmdCount++;
	}
	else if(m_rxData1[0] == (char)START_OF_HEADING && m_cmdCount == COMMAND_ONE)			//START OF HEADING COMMAND
	{
		m_rxBuffer[m_setDataLoc] = m_rxData1[0];
		m_setDataLoc++;
		m_cmdCount++;
	}
	else if(m_rxData1[0] == (char)END_OF_TRANSMISSION && m_cmdCount == COMMAND_TWO)		//END OF TRANSMISSION COMMAND
	{
		m_rxBuffer[m_setDataLoc] = m_rxData1[0];
	}
}

static void m_CheckSetCmd()
{
	if(m_rxData1[0] == (char)SET_ITEMS_CMD && m_cmdCount == COMMAND_ZERO)			//SET ITEMS COMMAND
	{
		m_LynqCom = SET_ITEMS_CMD;
		m_rxBuffer[m_setDataLoc] = m_rxData1[0];
		m_setDataLoc++;
		m_cmdCount++;
	}
	else if(m_cmdCount == COMMAND_ONE)												//RECEIVED DATA for name
	{
		if (m_rxBufferCounter < LYNQ_TOT_DATA_RX_SIZE-m_setDataLoc)
		{
			m_rxBuffer[m_setDataLoc+m_rxBufferCounter] = m_rxData1[0];
			m_rxBufferCounter++;
		}
	}
}

static void m_ProcessCommand(uint8_t var)
{
	uint8_t dataLoc = OFFSET;
	uint8_t storeCtr = 0;
	int eraseCtr = 0;

	int packetTxCtr = 0;
	int nameByteSt = 0;
	int freqByteSt = 0;
	int longByteSt = 0;
	int latByteSt = 0;

	switch (m_rxBuffer[dataLoc])
	{
		case GET_ITEMS_CMD:
			if ((m_rxBuffer[dataLoc+1] == (char)START_OF_HEADING) &&
				(m_rxBuffer[dataLoc+2] == (char)(char)END_OF_TRANSMISSION))
			{
				//erase tx buffer
				for (int i = 0; i < LYNQ_TOT_DATA_TX_SIZE; i++)
				{
					m_txBuffer[i] = 0x00;
				}

				//m_txBuffer[packetTxCtr++] = SET_ITEMS_CMD;	//We duplicate this command because the receive on the app
															//does the first byte and then the message.
				m_txBuffer[packetTxCtr++] = SET_ITEMS_CMD;
				m_txBuffer[packetTxCtr++] = START_OF_HEADING;
				m_txBuffer[packetTxCtr++] = START_OF_TEXT;
				m_txBuffer[packetTxCtr++] = m_lynqNameNumBytes;
				for (nameByteSt = packetTxCtr; packetTxCtr < nameByteSt + m_lynqNameNumBytes; packetTxCtr++)
				{
					m_txBuffer[packetTxCtr] = m_lynqName[storeCtr];		//store name
					storeCtr++;
				}
				storeCtr = 0;
				m_txBuffer[packetTxCtr++] =  END_OF_TEXT;
				m_txBuffer[packetTxCtr++] =  START_OF_TEXT;
				m_txBuffer[packetTxCtr++] = m_lynqFreqNumBytes;
				for (freqByteSt = packetTxCtr; packetTxCtr < freqByteSt + m_lynqFreqNumBytes; packetTxCtr++)
				{
					m_txBuffer[packetTxCtr] = m_lynqFreq[storeCtr];		//store frequency
					storeCtr++;
				}
				storeCtr = 0;
				m_txBuffer[packetTxCtr++] =  END_OF_TEXT;
				m_txBuffer[packetTxCtr++] =  START_OF_TEXT;
				m_txBuffer[packetTxCtr++] = m_lynqLongNumBytes;
				for (longByteSt = packetTxCtr; packetTxCtr < longByteSt + m_lynqLongNumBytes; packetTxCtr++)
				{
					m_txBuffer[packetTxCtr] = m_lynqLong[storeCtr];		//store longitude
					storeCtr++;
				}
				storeCtr = 0;
				m_txBuffer[packetTxCtr++] =  END_OF_TEXT;
				m_txBuffer[packetTxCtr++] =  START_OF_TEXT;
				m_txBuffer[packetTxCtr++] = m_lynqLatNumBytes;
				for (latByteSt = packetTxCtr; packetTxCtr < latByteSt + m_lynqLatNumBytes; packetTxCtr++)
				{
					m_txBuffer[packetTxCtr] = m_lynqLat[storeCtr];		//store latitude
					storeCtr++;
				}
				storeCtr = 0;
				m_txBuffer[packetTxCtr++] =  END_OF_TEXT;
				m_txBuffer[packetTxCtr++] =  END_OF_TRANSMISSION;

				m_txBuffer[packetTxCtr++] =  CARRIAGE_RETURN;
				m_txBuffer[packetTxCtr++] =  NEW_LINE;
				m_txBuffer[packetTxCtr++] =  TX_TERMINATOR;

				//send to APP
				HAL_UART_Transmit_IT(&huart1, m_txBuffer, LYNQ_TOT_DATA_TX_SIZE);

#ifdef DEBUG
				//for debugging
				HAL_UART_Transmit_IT(&huart3, m_txBuffer, LYNQ_TOT_DATA_TX_SIZE);
#endif
			}
			break;
		case SET_ITEMS_CMD:
			if ((m_rxBuffer[dataLoc+1] == (char)START_OF_HEADING) &&
				(m_rxBuffer[dataLoc+2] == (char)START_OF_TEXT))
			{

				//erase name storage before putting in data.
				for(eraseCtr = 0; eraseCtr < (uint8_t)LYNQ_NAME_SIZE; eraseCtr++)
				{
					m_lynqName[eraseCtr] = (uint8_t)0x00;
				}

				m_lynqNameNumBytes = m_rxBuffer[dataLoc+3];	//get Name Number of Bytes
				dataLoc+=4;	//point to location of the data elements

				for(storeCtr = 0; storeCtr < m_lynqNameNumBytes; storeCtr++)
				{
					m_lynqName[storeCtr] = m_rxBuffer[dataLoc];
					dataLoc++;
				}

				//array pointer at end should point to END_OF_TEXT element

				//TODO: check for END_OF_TEXT
				dataLoc++;	//point to location of the START_OF_TEXT
				//TODO: check for START_OF_TEXT
				dataLoc++;	//point to location of the Number of Arguments of Frequency bytes

				m_lynqFreqNumBytes = m_rxBuffer[dataLoc];	//get Frequency Number of Bytes
				dataLoc++; 	//point to location of the frequency Bytes

				//erase frequency storage before putting in data.
				for(eraseCtr = 0; eraseCtr < LYNQ_DATA_SIZE; eraseCtr++)
				{
					m_lynqFreq[eraseCtr] = 0x00;
				}

				for (storeCtr = 0; storeCtr < m_lynqFreqNumBytes; storeCtr++)
				{
					m_lynqFreq[storeCtr] = m_rxBuffer[dataLoc];	//store frequency
					dataLoc++;
				}

				//array pointer at end should point to END_OF_TEXT element

				//TODO: check for END_OF_TEXT
				dataLoc++;	//point to location of the START_OF_TEXT
				//TODO: check for START_OF_TEXT
				dataLoc++;	//point to location of the Number of Arguments of Longitude bytes

				m_lynqLongNumBytes = m_rxBuffer[dataLoc];	//get Longitude Number of Bytes
				dataLoc++;	//point to location of the Longitude bytes

				//erase longitude storage before putting in data.
				for(eraseCtr = 0; eraseCtr < LYNQ_DATA_SIZE; eraseCtr++)
				{
					m_lynqLong[eraseCtr] = 0x00;
				}

				for (storeCtr = 0; storeCtr < m_lynqLongNumBytes; storeCtr++)
				{
					m_lynqLong[storeCtr] = m_rxBuffer[dataLoc];	//store longitude
					dataLoc++;
				}

				//array pointer at end should point to END_OF_TEXT element

				//TODO: check for END_OF_TEXT
				dataLoc++;	//point to location of the START_OF_TEXT
				//TODO: check for START_OF_TEXT
				dataLoc++;	//point to location of the Number of Arguments of Latitude bytes

				m_lynqLatNumBytes = m_rxBuffer[dataLoc];	//get Latitude Number of Bytes
				dataLoc++;	//point to location of the Latitude bytes

				//erase latitude storage before putting in data.
				for(eraseCtr = 0; eraseCtr < LYNQ_DATA_SIZE; eraseCtr++)
				{
					m_lynqLat[eraseCtr] = 0x00;
				}

				for (storeCtr = 0; storeCtr < m_lynqLatNumBytes; storeCtr++)
				{
					m_lynqLat[storeCtr] = m_rxBuffer[dataLoc];	//store latitude
					dataLoc++;
				}

				//array pointer at end should point to END_OF_TEXT element

				//TODO: check for END_OF_TEXT
				//TODO: check for END OF TRANSMISSION

				//TODO: any code for data being valid at this point TBD
			}
			break;
	}

	//Resets receive buffers and byte counting elements.
	m_LynqCom = INVALID_CMD;
	m_setDataLoc = OFFSET;
	m_rxBufferCounter = 0;
	m_cmdCount = COMMAND_ZERO;

	for (uint8_t bufferCtr = 0; bufferCtr < LYNQ_TOT_DATA_RX_SIZE; bufferCtr++)
	{
		m_rxBuffer[bufferCtr] = 0x00;
	}
}

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(char *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
